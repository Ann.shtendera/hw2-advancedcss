import gulp from "gulp";
const { src, dest, watch, series, parallel } = gulp;

import imagemin from 'gulp-imagemin';
import cleanCss from "gulp-clean-css";
import terser from "gulp-terser";
import concat from "gulp-concat";
import autoprefixer from "gulp-autoprefixer";
import purgecss from "gulp-purgecss";
import clean from "gulp-clean";

import dartSass from 'sass';
import gulpSass from 'gulp-sass';
const sass = gulpSass(dartSass);

import bsc from 'browser-sync';
const browserSync = bsc.create();


const cleanDist = () => {
	return src('./dist', {read: false})
	.pipe(clean());
};

const cleanJS = () => {
	return src('./dist/js', {read: false})
	.pipe(clean());
};

const cleanCSS = () => {
	return src('./dist/css', {read: false})
	.pipe(clean());
};


const minifyImg = () => {
	return src('./src/img/**/*')
	.pipe(imagemin())
	.pipe(dest('./dist/img'))
};

const compileCSS = () => {
	return src('./src/scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
	.pipe(purgecss({
		content: ['./index.html', './src/js/**/*.js']
	}))
	.pipe(autoprefixer())
    .pipe(concat('styles.min.css'))
    .pipe(cleanCss())
    .pipe(dest('./dist/css'))
	.pipe(browserSync.stream());
}

const minifyJS = () => {
	return src('./src/js/**/*.js')
	.pipe(concat('scripts.min.js'))
	.pipe(terser()) 
	.pipe(dest('./dist/js'))
};

const startWatch = () => {
	browserSync.init({
		server: {
			baseDir: "./"
		},
	});

	watch('./src/scss/**/*.scss').on('all', gulp.series(cleanCSS, compileCSS, browserSync.reload))
	watch('./src/js/**/*.js').on('all', gulp.series(cleanJS, minifyJS, browserSync.reload))

	watch('./index.html').on('change', browserSync.reload);
	watch('./src/img/**/*').on('all', gulp.series(minifyImg, browserSync.reload));
};

export const cleaning = cleanDist;
export const img = minifyImg;
export const css = compileCSS;
export const js = minifyJS;

export const build = series(cleanDist, parallel(compileCSS, minifyJS, minifyImg));
export const dev = series(build, startWatch);