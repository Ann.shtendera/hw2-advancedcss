const burgerBtn = document.querySelector('.header__burger-btn');
const dropDownMenu = document.querySelector('.header__navigation');
const icons = document.querySelectorAll('.header__btn-icon')

burgerBtn.addEventListener('click', (e) => {
	dropDownMenu.classList.toggle('drop-down');
	icons.forEach(icon => {
		icon.classList.toggle('header__btn-icon--active');
	})
})